/**
 * 常用方法封装 请求，文件上传等
 * @author echo. 
 **/
import config from './config'
const tui = {
	//评估模块接口地址
	interfaceUrl: function() {
	//#ifdef H5
		return config.apiUrl//测试环境
	//#endif
	//#ifndef H5
		 return config.apiUrl//测试环境
	//#endif
	},
	//MALL接口地址
	interfaceMallUrl: function() {
	//#ifdef H5
		return config.apiMallUrl//测试环境
	//#endif
	//#ifndef H5
		 return config.apiMallUrl//测试环境
	//#endif
	},
	//IM接口地址
	interfaceImUrl: function() {
	//#ifdef H5
		return config.apiImUrl//测试环境
	//#endif
	//#ifndef H5
		 return config.apiImUrl//测试环境
	//#endif
	},
	/**
	 * 请求数据处理
	 * @param string url 请求地址
	 * @param string method 请求方式
	 *  GET or POST
	 * @param {*} postData 请求参数
	 * @param bool isDelay 是否延迟显示loading
	 * @param bool isForm 数据格式
	 *  true: 'application/x-www-form-urlencoded'
	 *  false:'application/json'
	 * @param bool hideLoading 是否隐藏loading
	 *  true: 隐藏
	 *  false:显示
	 */
	delayed: null,
	request: function(url, method, postData, isDelay, isForm, hideLoading, mall) {
		//接口请求
		let loadding = false;
		tui.delayed && uni.hideLoading();
		clearTimeout(tui.delayed);
		tui.delayed = null;
		if (!hideLoading) {
			tui.delayed = setTimeout(() => {
				uni.showLoading({
					mask: true,
					title: '请稍候...',
					success(res) {
						loadding = true
					}
				})
			}, isDelay ? 1000 : 0)
		}
		return new Promise((resolve, reject) => {
			let mainUrl = ''
			if(mall == 'mall'){
				mainUrl = tui.interfaceMallUrl() + url
			}else if(mall == 'im'){
				mainUrl = tui.interfaceImUrl() + url
			}else{
				mainUrl = tui.interfaceUrl() + url
			}
			uni.request({
				url: mainUrl,
				data: postData,
				header: {
					'content-type': isForm ? 'application/x-www-form-urlencoded' : 'application/json',
					'Authorization': 'Bearer '+ tui.getToken()
				},
				method: method, //'GET','POST'
				dataType: 'json',
				success: (res) => {
					clearTimeout(tui.delayed)
					tui.delayed = null;
					if (loadding && !hideLoading) {
						uni.hideLoading()
					}
					resolve(res.data)
				},
				fail: (res) => {
					clearTimeout(tui.delayed)
					tui.delayed = null;
					tui.toast("网络不给力，请稍后再试~")
					reject(res)
				}
			})
		})
	},
	//跳转页面，校验登录状态
	href(url, isVerify) {
		if (isVerify && !tui.isLogin()) {
			uni.navigateTo({
				url: '/pages/login/login'
			})
		} else {
			uni.navigateTo({
				url: url
			});
		}
	},
	toast: function(text, duration, success) {
		uni.showToast({
			title: text || "出错啦~",
			icon: success ? 'success' : 'none',
			duration: duration || 2000
		})
	},
	modal: function(title, content, showCancel, callback, confirmColor, confirmText) {
		uni.showModal({
			title: title || '提示',
			content: content,
			showCancel: showCancel,
			cancelColor: "#555",
			confirmColor: confirmColor || "#5677fc",
			confirmText: confirmText || "确定",
			success(res) {
				if (res.confirm) {
					callback && callback(true)
				} else {
					callback && callback(false)
				}
			}
		})
	},
	isAndroid: function() {
		const res = uni.getSystemInfoSync();
		return res.platform.toLocaleLowerCase() == "android"
	},
	isPhoneX: function() {
		const res = uni.getSystemInfoSync();
		let iphonex = false;
		let models=['iphonex','iphonexr','iphonexsmax','iphone11','iphone11pro','iphone11promax']
		const model=res.model.replace(/\s/g,"").toLowerCase()
		if (models.includes(model)) {
			iphonex = true;
		}
		return iphonex;
	},
   //设置用户信息
	setWechatInfo: function(unionId, openId) {
		uni.setStorageSync("unionId", unionId)
		uni.setStorageSync("openId", openId)
	},
	/* 将token放入缓存 */
	setToken:function(token) {
		uni.setStorageSync("token", token)
	},
	//获取token
	getToken() {
		return uni.getStorageSync("token")
	},
	//获取用户信息
	userInfo: function() {
		let data={};
		return new Promise((resolve, reject) => {
			this.request('mall-portal/sso/info', 'GET', data, false, false, true,'mall').then((res) => {
				console.log(res)
				 if(res.code == '200'){
					 resolve(res.data)
				 }else{
					 console.log(res.msg)
				 }
			})
		})
	},
	//提示框
	showToast(text,type,url,way){
		uni.showToast({
			title:text,
			icon:type,
			duration:2000
		})
		if(url){
			if(way && way == 'navigateBack'){
				setTimeout(function() {
					 uni.navigateBack({
					 	delta: 1
					 })
				}, 1800)
			}else{
				setTimeout(function() {
					 uni.redirectTo({
						url:url
					 })
				}, 1800)
			}
		}
		
	}
}

export default tui
