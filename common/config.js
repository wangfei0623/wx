
const config = {
	/* 接口地址配置 */
	// apiUrl: 'http://192.168.0.200:8080/',//本地环境若依
	// apiMallUrl: 'http://192.168.0.200:8201/',//本地环境mall
	// apiImUrl:'http://192.168.0.200:8080/service/api/service/',//本地环境服务地址
	apiUrl: 'https://wx.shufuzhilian.com/dpm-api/',//测试环境若依
	apiMallUrl: 'https://wx.shufuzhilian.com/mall-api/',//测试环境mall
	apiImUrl:'https://wx.shufuzhilian.com/dpm-api/service/api/service/',//测试环境mall服务地址
	/* minio 地址配置*/
	minioUrl:'https://minio.zykj.link/public/wujiayi-weapp/',//minio静态资源地址
	/* 平台客服电话 */
	kefu:'4000881551',
	/* 本小程序appid */
	appId:'wxc64c27eeb9516624',
	/* 手机号验证 */
	phoneReg : /^(14[0-9]|13[0-9]|15[0-9]|16[0-9]|17[0-9]|18[0-9]|19[0-9])\d{8}$$/,
}
export default config